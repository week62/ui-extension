import 'package:flutter/material.dart';

class dropDownWidget extends StatefulWidget {
  dropDownWidget({Key? key}) : super(key: key);

  @override
  _dropDownWidgetState createState() => _dropDownWidgetState();
}

class _dropDownWidgetState extends State<dropDownWidget> {
String vaccine = '-';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('DropDown')),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left:15.0,right: 15.0, top: 10.0,bottom: 10.0),
            child: Row(children: [
              Text('Vaccine'),
              Expanded(child: Container()),
              DropdownButton(
                items: [
                  DropdownMenuItem(child: Text('-'), value: '-'),
                  DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                  DropdownMenuItem(child: Text('Johnson & Johnson'), value: 'Johnson & Johnson'),
                  DropdownMenuItem(child: Text('Sputnik V'), value: 'Sputnik V'),
                  DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca'),
                  DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
                  DropdownMenuItem(child: Text('Sinioharm'), value: 'Sinioharm'),
                  DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
                ],
                value: vaccine,
                onChanged: (String? value){
                  setState(() {
                    vaccine = value!;
                  });
                },)
            ],),
          ),
          Center(child: Text('Your chosen vaccine: $vaccine', 
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.w800),),)
      ],),
    );
  }
}