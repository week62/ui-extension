import 'package:flutter/material.dart';

Widget _checkBox(String title,bool value,ValueChanged<bool?>?onCheanged){
  return Container(
              padding: EdgeInsets.only(left:15.0,right: 15.0, top: 10.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(title),
                      Expanded(
                        child: Container(),
                      ),
                      Checkbox(
                          value: value,
                          onChanged: onCheanged
                      )],
                  ),
                  //ขีดเส้น
                  Divider()
                ],));
}

class checkBoxWidget extends StatefulWidget {
  checkBoxWidget({Key? key}) : super(key: key);

  @override
  _checkBoxWidgetState createState() => _checkBoxWidgetState();
}

class _checkBoxWidgetState extends State<checkBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ComboBox'),
      ),
      body: ListView(
        children: [
          _checkBox('check 1',check1,(value){
            setState(() {
              check1 = value!;
            });
            },),
            _checkBox('check 2',check2,(value){
            setState(() {
              check2 = value!;
            });
            },
          ),
          _checkBox('check 3',check3,(value){
            setState(() {
              check3 = value!;
            });
            },
          ),
          TextButton(child: Text('Save'),
          onPressed: (){
            ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(
              'check 1: $check1, check 2: $check2, check 3: $check3'),));
          }, )
        ],
      ),
    );
  }
}
