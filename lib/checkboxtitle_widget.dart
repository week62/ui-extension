import 'package:flutter/material.dart';

class checkBoxTitleWidget extends StatefulWidget {
  checkBoxTitleWidget({Key? key}) : super(key: key);

  @override
  _checkBoxTitleWidget createState() => _checkBoxTitleWidget();
}

class _checkBoxTitleWidget extends State<checkBoxTitleWidget> {
bool check1 =false;
bool check2 = false;
bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Scaffold(
      appBar: AppBar(
        title: Text('ComboBoxTitle'),
      ),
      body: ListView(
        children: [
          CheckboxListTile(
            title: Text('check 1'),
            subtitle: Text('sub check 1'),
            value: check1, onChanged: (value){
            setState(() {
              check1 = value!;
            });
          }),
          CheckboxListTile(
            title: Text('check 2'),
            subtitle: Text('sub check 2'),
            value: check2, onChanged: (value){
            setState(() {
              check2 = value!;
            });
          }),
          CheckboxListTile(
            title: Text('check 3'),
            subtitle: Text('sub check 3'),
            value: check3, onChanged: (value){
            setState(() {
              check3 = value!;
            });
          }),
          TextButton.icon(onPressed: (){
            ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(
              content: Text('check 1: $check1, check 2: $check2, check 3: $check3'),));
          }, 
          icon: Icon(Icons.save), 
          label: Text('Save'))
        ],
      ),
    ));
  }
}
