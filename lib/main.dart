import 'package:flutter/material.dart';

import 'checkbox_widget.dart';
import 'checkboxtitle_widget.dart';
import 'dropdown_widget.dart';
import 'radio_widget.dart';

void main() {
  runApp(MaterialApp(
    title: 'Ui Extension',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Ui Extension')),

      //drawer
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(
              child: Text('UI MENU'),
              decoration: BoxDecoration(color: Colors.blue),
            ),
            ListTile(
              title: Text('CheckBox'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => checkBoxWidget()));
              },
            ),
            //checkBoxTitle
            ListTile(
              title: Text('CheckBoxTitle'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => checkBoxTitleWidget()));
              },
            ),
            //dropDown
            ListTile(
              title: Text('DropDown'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => dropDownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => radioWidget()));
              },
            )
          ],
        ),
      ),

      //body
      body: ListView(
        children: [
          //checkBox
          ListTile(
            title: Text('CheckBox'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => checkBoxWidget()));
            },
          ),
          //checkBoxTitle
          ListTile(
            title: Text('CheckBoxTitle'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => checkBoxTitleWidget()));
            },
          ),
          //dropDown
          ListTile(
            title: Text('DropDown'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => dropDownWidget()));
            },
          ),
          ListTile(
            title: Text('Radio'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => radioWidget()));
            },
          ),
        ],
      ),
    );
  }
}
