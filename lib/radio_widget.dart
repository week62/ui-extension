import 'package:flutter/material.dart';

class radioWidget extends StatefulWidget {
  radioWidget({Key? key}) : super(key: key);

  @override
  _radioWidgetState createState() => _radioWidgetState();
}

class _radioWidgetState extends State<radioWidget> {
  int val = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Radio'),),
      body: ListView(children: [
        //Radio 1
        ListTile(
          leading: Radio(
            groupValue: val,value: 1,onChanged: (int? value){
              setState(() {
                val = value!;
              });
            },),
            title: Text('One'),
        ),Divider(),
        ListTile(
          leading: Radio(
            groupValue: val,value: 2,onChanged: (int? value){
              setState(() {
                val = value!;
              });
            },),
            title: Text('Two'),
        ),Divider(),
        ListTile(
          leading: Radio(
            groupValue: val,value: 3,onChanged: (int? value){
              setState(() {
                val = value!;
              });
            },),
            title: Text('Tree'),
        ),Divider(),
        TextButton.icon(
        onPressed: (){
          ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Select $val')));
        }, 
        icon: Icon(Icons.save), 
        label: Text('Save'))
      ],),
    );
  }
}